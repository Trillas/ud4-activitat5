
public class Activitat5 {

	public static void main(String[] args) {
		//Declaro las variables
		int a = 5,b = 2,c = 8,d = 7;
		//Hago las operaciones
		System.out.println("valor inicial de a:" + a + " b:" + b + " c:" + c + " d:" + d);
		b = c;
		c = a;
		a = d;
		d = b;
		System.out.println("valor final de a:" + a + " b:" + b + " c:" + c + " d:" + d);
	}

}
